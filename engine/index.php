<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

define('SYSTEM_STARTED', TRUE);
define('PRODUCTION', FALSE);

require_once('core/bootstrap.php');

$uriParams=getURIParameters();

/**
 * Explicitly set command for default view.
 */
if(count($uriParams)==0) {
	$action='default';
} else {
	$action=$uriParams[0];
	$uriParams=array_slice($uriParams,1);
}

/**
 * Resolve URI request.
 */
resolve($action,$uriParams);

?>
