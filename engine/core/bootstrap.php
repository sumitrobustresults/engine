<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

defined('SYSTEM_STARTED') or die('You are not permitted to access this resource.');

require_once('vendor/_required/klogger-1.0.0/KLogger.php');

$LOGGER = new KLogger('kiln.log', KLogger::DEBUG);

if(PRODUCTION) {
	register_shutdown_function('on_shutdown');
	set_error_handler('on_error');
}

// Load environment variables
$envvars = explode("\n", file_get_contents('/etc/environment'));
foreach($envvars as $env) {
	if(!$env) continue;
	$pair = explode("=", $env);
	$_SERVER[$pair[0]] = $pair[1];
}

if(!file_exists('app/config/tools.ini'))
	file_put_contents('app/config/tools.ini', '');

require_once('core/globals.php');
require_once('core/autoload.php');
require_once('core/common.php');
require_once('core/resolver.php');

require_once('vendor/autoload.php');

spl_autoload_register('load_system');
spl_autoload_register('load_module');
spl_autoload_register('load_controller');
spl_autoload_register('load_redbean');
spl_autoload_register('load_swiftmailer');

function on_error($error_level, $error_message, $error_file, $error_line, $error_context = null) {

	global $LOGGER;

	$log_message = sprintf('[LEVEL: %s]::[FILE: %s]::[LINE: %s]::[MESSAGE: %s]',
			$error_level, $error_file, $error_line, $error_message);

	$LOGGER->LogError($log_message);
	
	die('An error occurred. Could not continue.');
}

function on_shutdown() {

	$error = error_get_last();

	if($error) {
		$type = $error['type'];
		$message = $error['message'];
		$file = $error['file'];
		$line = $error['line'];
		on_error($type, $message, $file, $line);
	}
}
	
?>
