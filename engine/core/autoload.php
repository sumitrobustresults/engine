<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */
# Here we are loadin all files of app/modules folder, system folder and  app/controllers
defined('SYSTEM_STARTED') or die('You are not permitted to access this resource.');

function load_module($class_name) {
	
	$file_name=PATH_MODULES.$class_name.'.php';
	
	if(is_readable($file_name)) {
		require_once($file_name);
		if(method_exists($class_name,'init')) $class_name::init();
	}
}

function load_system($class_name) {
	
	$file_name=PATH_SYSTEM.$class_name.'.php';
	
	if(is_readable($file_name)) {
		require_once($file_name);
		if(method_exists($class_name,'init')) $class_name::init();
	}
}

function load_controller($class_name) {
	
	$file_name=PATH_CONTROLLERS.$class_name.'.php';
	
	if(is_readable($file_name)) {
		require_once($file_name);
		if(method_exists($class_name,'init')) $class_name::init();
	}
}

/*
 * Required environment variables:
 * 1. DB_HOST
 * 2. DB_PORT
 * 3. DB_NAME
 * 4. DB_USER
 * 5. DB_PASS
 */
function load_redbean($class_name) {
	
	if($class_name !== 'R') return;
	
	require_once('vendor/_required/redbeanphp-3.5.4/rb.php');
	
	$db_host=$_SERVER['DB_HOST'];
	$db_port=$_SERVER['DB_PORT'];
	$db_user=$_SERVER['DB_USER'];
	$db_pass=$_SERVER['DB_PASS'];
	$db_name=$_SERVER['DB_NAME'];
	
	R::setup(sprintf('mysql:host=%s;port=%s;dbname=%s',$db_host,$db_port,$db_name),$db_user,$db_pass);
}

function load_swiftmailer($class_name) {
	
	if(strpos($class_name, 'Swift') != 0) return;
	
	require_once('vendor/_required/swift-5.1.0/lib/swift_required.php');
}

?>
