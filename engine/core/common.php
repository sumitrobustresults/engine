<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

defined('SYSTEM_STARTED') or die('You are not permitted to access this resource.');

function getURIParameters() {
	
	$uriResource=explode('?',$_SERVER['REQUEST_URI']);
	$scriptResource=explode('?',$_SERVER['SCRIPT_NAME']);
	
	$requestURI=explode('/',$uriResource[0]);
	$scriptName=explode('/',$scriptResource[0]);

	for($i=0;$i<sizeof($scriptName);$i++) { if($requestURI[$i]==$scriptName[$i]) unset($requestURI[$i]); }
	
	$requestURI=array_filter($requestURI);
	$requestParams=array_values($requestURI);
	
	//$requestParams=array_merge($requestParams,$_REQUEST);
	
	return $requestParams;
}

function generate_random_string($length = 25) {
	
	if($length < 5) $length = 5;
	
	$pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	$chunk_size = 5;
	
	$num_chunks = intval($length / $chunk_size);
	$extra = $length % $chunk_size;
	
	$random_string = '';
	
	for($index = 0; $index < $num_chunks; $index++) {
		$chunk = substr(str_shuffle($pool), 0, $chunk_size);
		$random_string = $random_string.$chunk;
	}
	
	$chunk = substr(str_shuffle($pool), 0, $extra);
	$random_string = $random_string.$chunk;
	
	return $random_string;
}
	
?>
