<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

/**
 * ----------------------------------
 * GLOBAL CONSTANTS ARE DEFINED HERE.
 * ----------------------------------
 */

defined('SYSTEM_STARTED') or die('You are not permitted to access this resource.');

define('DS',DIRECTORY_SEPARATOR);

define('BASE_DIR', getcwd().'/');
define('BASE_URI', '/');	

define('PATH_SYSTEM', 'system/');
define('PATH_MODULES', 'app/modules/');
define('PATH_CONTROLLERS', 'app/controllers/');
define('PATH_THIRD_PARTY', 'app/external/');
define('PATH_APPDATA', 'data/');
	
?>
