CWD=$(pwd)
 
apt-get -y install git-core
apt-get -y install build-essential
apt-get -y install python2.7-dev
apt-get -y install python-setuptools
apt-get -y install wget
 
wget https://pypi.python.org/packages/source/p/pycparser/pycparser-2.10.tar.gz
wget https://pypi.python.org/packages/source/p/python-Levenshtein/python-Levenshtein-0.11.2.tar.gz
 
tar -xvf pycparser-2.10.tar.gz
tar -xvf python-Levenshtein-0.11.2.tar.gz
 
cd pycparser-2.10
python setup.py install
cd ..
cd python-Levenshtein-0.11.2
python setup.py install
cd ..

git clone https://github.com/openjudge/sandbox.git

cd sandbox/libsandbox
./configure
make install
cd ../../

ln -s /usr/local/lib/libsandbox.so /usr/lib/libsandbox.so

rm -rf sandbox
 
apt-get -y install apache2
apt-get -y install php5
#apt-get -y install libapache2-mod-php5
#apt-get -y install mysql-server
apt-get -y install php5-mysql
 
a2enmod rewrite

echo -e "\nServerName localhost" >> /etc/apache2/apache2.conf
sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
sed -i 's/VirtualHost \*:80/VirtualHost \*:8080/g' /etc/apache2/sites-available/000-default.conf
sed -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/its-engine/g' /etc/apache2/sites-available/000-default.conf
 
chown -R $(whoami) /var/www
chgrp -R www-data /var/www
chmod -R 771 /var/www

add-apt-repository ppa:chris-lea/node.js
apt-get -y update
apt-get -y install nodejs
 
npm install -g express
npm install -g forever

service apache2 restart

apt-get -y install nginx

cd $CWD 
cp ./default /etc/nginx/sites-available/default
 
apt-get -y install redis-server

cd /var/www

git clone https://rajd33pd@bitbucket.org/rajd33pd/its-engine.git
git clone https://rajd33pd@bitbucket.org/rajd33pd/its-web.git

cp /var/www/its-engine/app/config/app.ini.default /var/www/its-engine/app/config/app.ini
touch /var/www/its-engine/kiln.log

cd /var/www/its-engine
php composer.phar install

forever start /var/www/its-web/bin/www
service nginx restart
 
echo "Setup complete. You must configure /var/www/its-engine/app/config/app.ini and /var/www/its-web/app.js before you run this app."
