CWD=$(pwd)

mkdir ~/.repo

cd ~/.repo

git init --bare its-web.git
git init --bare its-engine.git

cd $CWD

cp post-receive ~/.repo/its-web.git/hooks/
sed -i 's/its/its-web/g' ~/.repo/its-web.git/hooks/post-receive

cp post-receive ~/.repo/its-engine.git/hooks/
sed -i 's/its/its-engine/g' ~/.repo/its-engine.git/hooks/post-receive

cd /var/www/its-web
git remote add local ~/.repo/its-web.git

cd /var/www/its-engine
git remote add local ~/.repo/its-engine.git
