tar -xvf mysql-cluster-gpl-7.3.6-linux-glibc2.5-x86_64.tar.gz

cp -R mysql-cluster-gpl-7.3.6-linux-glibc2.5-x86_64 /usr/local/mysql

cd /usr/local/mysql

cp bin/ndb_mgm* /usr/local/bin/

chmod +x /usr/local/bin/ndb_mgm*

mkdir /var/lib/mysql-cluster
cp config.ini /var/lib/mysql-cluster/

echo "Management node set up successfully on this server."
