groupadd mysql
useradd -g mysql mysql

tar -xvf mysql-cluster-gpl-7.3.6-linux-glibc2.5-x86_64.tar.gz

cp -R mysql-cluster-gpl-7.3.6-linux-glibc2.5-x86_64 /usr/local/mysql

cd /usr/local/mysql

apt-get -y install libaio1 libaio-dev

scripts/mysql_install_db --user=mysql

chown -R root .
chown -R mysql data
chgrp -R mysql .

cp support-files/mysql.server /etc/init.d/
chmod +x /etc/init.d/mysql.server
update-rc.d mysql.server defaults

cp bin/ndbd /usr/local/bin/
cp bin/ndbmtd /usr/local/bin

chmod +x /usr/local/bin/ndb*

cp my.cnf /etc/

echo "SQL and Data nodes set up successfully on this server."
