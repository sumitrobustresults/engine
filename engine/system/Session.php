<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

defined('SYSTEM_STARTED') or die('You are not permitted to access this resource.');

use Predis\Client as PredisClient;

class Session {
	
	private static $SECRET = 'fsajfb3f934hg93hg';
	
	private static $user = null;
	private static $event = null;
	
	public static function init() {
		
		$TYPE = $_SERVER['CACHE_TYPE'];
		$HOST = $_SERVER['CACHE_ADDR'];
		$PORT = $_SERVER['CACHE_PORT'];
		
		$hosts = json_decode($HOST, true);
		if($hosts && is_array($hosts)) {
			$HOST = $hosts;
		}
		
		// TODO Clustering support.
		if($TYPE === 'REDIS') {
			if(!isset($_COOKIE['connect_sid'])) return;
			$sid = $_COOKIE['connect_sid'];
			$sid = str_replace("s:", "", $sid);
			$sid = substr($sid, 0, strrpos($sid, "."));
			$key = 'sess:' . $sid;
			$redis = new PredisClient(array(
				'scheme'=>'tcp',
				'host'=>$HOST,
				'port'=>$PORT
			));
			$session = json_decode($redis->get($key), true);
		} else {
			if(!isset($_COOKIE['its'])) return;
			$sid = $_COOKIE['its'];
			$sid = str_replace("s:", "", $sid);
			$sid = substr($sid, 0, strrpos($sid, "."));
			$key = 'sess:' . $sid;
			$memcached = new Memcached();
			// Servers
			$servers = array();
			if(is_array($HOST)) {
				foreach($HOST as $h)
					array_push($servers, array($h, intval($PORT)));
			} else 
				array_push($servers, array($HOST, intval($PORT)));
			$memcached->addServers($servers);
			$memcached->setOptions(array(
				Memcached::OPT_HASH => Memcached::HASH_MD5,
				Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
				Memcached::OPT_LIBKETAMA_COMPATIBLE => true
			));
			$session = json_decode($memcached->get($key), true);
		}
		
		if(!$session) return;
		
		if(isset($session['user'])) {
			$user = $session['user'];
			$user['role'] = $session['role'];
		} else {
			$user = null;
		}
		
		self::$user = $user;
		if(isset($session['now']))
			self::$event = $session['now'];
	}
	
	public static function getUserID() {
		
		if(self::$user == null) return null;
		
		return self::$user['id'];
	}
	
	public static function getName() {
		
		if(self::$user == null) return null;
		
		return self::$user['full_name'];
	}
	
	public static function getRole() {
		
		if(self::$user == null) return null;
		
		return self::$user['role'];
	}
	
	public static function getEventNow() {
		
		if(self::$event == null) return null;
		
		return self::$event;
	}
}

?>
