<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

defined('SYSTEM_STARTED') or die('You are not permitted to access this resource.');

class Registry {
	
	const PORT_TYPE_PUBLIC = "PUBLIC";
	const PORT_TYPE_PRIVATE = "PRIVATE";
	
	/**
	 * Contains registry entries of the form:
	 * (<controller_name>,<method_name>)
	 */
	private static $port_registry=array();
	
	/**
	 * Contains registry entries of the form:
	 * <key>=<value>
	 */
	private static $custom_config = array();
	
	/**
	 * Contains the configurations for the external tools.
	 * @var mixed
	 */
	private static $tools_config = array();
	
	public static function init() {
		
		$config_ports = parse_ini_file(BASE_DIR.'app/config/ports.ini', TRUE);
		$config_custom = parse_ini_file(BASE_DIR.'app/config/custom.ini', FALSE);
		$config_tools = parse_ini_file(BASE_DIR.'app/config/tools.ini', TRUE);
		
		self::$port_registry = $config_ports;
		
		foreach(array_keys($config_ports['PUBLIC']) as $key) {
			$value = trim($config_ports['PUBLIC'][$key]);
			if($value) self::$port_registry['PUBLIC'][$key] = explode(':', $value);
			else self::$port_registry['PUBLIC'][$key] = null;
		}
		
		foreach(array_keys($config_ports['PRIVATE']) as $key) {
			$value = trim($config_ports['PRIVATE'][$key]);
			if($value) self::$port_registry['PRIVATE'][$key] = explode(':', $value);
			else self::$port_registry['PRIVATE'][$key] = null;
		}
		
		self::$custom_config = $config_custom;
		self::$tools_config = $config_tools;
	}
	
	public static function portExists($portName) {
		
		if(array_key_exists($portName, self::$port_registry['PUBLIC'])) return TRUE;
		if(array_key_exists($portName, self::$port_registry['PRIVATE'])) return TRUE;
		
		return FALSE;
	}
	
	public static function lookupPort($portName) {
		
		if(isset(self::$port_registry['PUBLIC'][$portName])) return array_merge(self::$port_registry['PUBLIC'][$portName], array('PUBLIC'));
		else if(isset(self::$port_registry['PRIVATE'][$portName])) return array_merge(self::$port_registry['PRIVATE'][$portName], array('PRIVATE'));
		else return null;
	}
	
	public static function listPorts() {
		
		return array_merge(array_keys(self::$port_registry['PUBLIC']), array_keys(self::$port_registry['PRIVATE']));
	}
	
	public static function lookupCustomConfig($key) {
		
		if(array_key_exists($key, self::$custom_config)) return self::$custom_config[$key];
		else return null;
	}
	
	public static function getHooks($event) {
		
		$hooks = array();
		
		$keys = array_keys(self::$tools_config);
		
		foreach($keys as $key) {
			$cfg = self::$tools_config[$key];
			if(isset($cfg[$event]) && $cfg[$event])
				array_push($hooks, array($key, $cfg[$event]));
		}
		
		return $hooks;
	}
	
	public static function settings($section, $key) {
		
		$CONFIG_FILE = BASE_DIR . 'app/config/custom.ini';
		
		$config = Helper::cacheData("SELECT setting,value FROM configuration WHERE section=? AND setting=?", array($section, $key), 60);
		if(!count($config))
			$config = R::getAssocRow("SELECT setting,value FROM configuration WHERE section=? AND setting=?", array($section, $key));
		
		if(!count($config)) {
			$config = parse_ini_file($CONFIG_FILE, true);
			if(isset($config[$section][$key])) {
				R::exec("INSERT INTO configuration (section,setting,value) VALUES (?,?,?)",
					array($section, $key, json_encode($config[$section][$key])));
				$value = $config[$section][$key];
			} else 
				$value = null;
		} else {
			$value = json_decode($config[0]['value'], true);
		}
		
		return $value;
	}
	
}

?>
