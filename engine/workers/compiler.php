<?php

/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

////////////////////// BOOTSTRAPPING //////////////////////////

define('SYSTEM_STARTED', TRUE);
define('PRODUCTION', FALSE);

require_once('core/bootstrap.php');

////////////////////// BOOTSTRAPPING //////////////////////////

$POLL_INTERVAL = 2;
$queue = Queue::instance();

while(true) {
	
	$job = $queue->retrieve('COMPILE');
	
	if($job) {
		
		$ref = $job['reference'];
		$code = $job['payload'];
		
		$build = md5(rand() . uniqid('', true));
		
		$result = Engine::instance()->compile($build, $code);
		$queue->complete('COMPILE', $ref, json_encode($result));
	}
	
	sleep($POLL_INTERVAL);
}

/*$numPending = Queue::instance()->getPending('COMPILE');

echo "Pending: $numPending\n";

if($numPending > 0) {
	
	$job = Queue::instance()->retrieve('COMPILE');
	$ref = $job['reference'];
	$payload = $job['payload'];
	
	echo "Reference: $ref\n";
	echo "Payload:\n$payload\n";
	
	Queue::instance()->remove('COMPILE', $ref);
}*/

?>