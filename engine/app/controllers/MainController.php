<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class MainController {
	
	public function renderDefault() {
		echo "No request received"; 
	}
	
	public function test() {
		
		if(!isset($_REQUEST['action']))
			die("no action specified");
		
		$action = $_REQUEST['action'];
		
		if($action === 'compile') {
			echo json_encode(Tests::instance()->testCompile());
		} else if($action === 'execute') {
			echo json_encode(Tests::instance()->testExecute());
		} else if($action === 'evaluate') {
			echo json_encode(Tests::instance()->testEvaluate());
		} else if($action === 'dbwrite') {
			echo json_encode(Tests::instance()->testDatabaseWrite());
		} else if($action === 'dbread') { 
			echo json_encode(Tests::instance()->testDatabaseRead());
		} else {
			echo "invalid test";
		}
	}
	
	public function tool() {
		
		$this->checkAccess();
		
		if(!isset($_REQUEST['action']))
			die("no action specified");
		
		$action = $_REQUEST['action'];
		
		if($action === 'tokenize') {
			$source = $_REQUEST['source'];
			echo json_encode(Analytics::instance()->tokenizeSource(base64_decode($source)));
		} else {
			echo "invalid action";
		}
	}
	
	public function compile() {
		
		$this->checkAccess();
		
		if(isset($_POST['scratch'])) $scratch = true;
		else $scratch = false;
           $code = $_POST['code'];
		
		if((Session::getRole() === 'admin' && !isset($_POST['assignment_id'])) || $scratch) {
			$admin = new Admin();
			$isAdmin = (Session::getRole() === 'admin'); 
			echo json_encode($admin->compile(base64_decode($code), $isAdmin));
		} else {
			$assignment_id = $_POST['assignment_id'];
			if(!$this->isAllowed($assignment_id)) {
				header('Location: /');
				return;
			}
			$judge = new Judge();
			echo json_encode($judge->compile($assignment_id, $code));
		}
	}
	
	public function execute() {
		
		$this->checkAccess();
		
		$execType = Registry::lookupCustomConfig('type');
		
		if(isset($_POST['scratch'])) $scratch = true;
		else $scratch = false;
		
		if((Session::getRole() === 'admin' && !isset($_POST['assignment_id'])) || $scratch) {
			$admin = new Admin();
			$isAdmin = (Session::getRole() === 'admin');
			if($execType === 'interpreted')
				echo json_encode($admin->interpret($_POST['code'], $_POST['testcase'], $isAdmin));
			else
				echo json_encode($admin->execute($_POST['code'], $_POST['testcase'], $isAdmin));
		} else {
			$assignment_id = $_POST['assignment_id'];
			if(!$this->isAllowed($assignment_id)) {
				header('Location: /');
				return;
			}
			$testcase = $_POST['testcase'];
			$judge = new Judge();
			if($execType === 'interpreted')
				echo json_encode($judge->interpret($assignment_id, $_POST['code'], $testcase));
			else
				echo json_encode($judge->execute($assignment_id, $testcase));
		}
	}
	
	public function evaluate() {
		
		$this->checkAccess();
		
		if(Session::getRole() === 'admin' && isset($_POST['admin']) && $_POST['admin'] === 'true') {
			$admin = new Admin();
			$isAdmin = (Session::getRole() === 'admin');
			if(isset($_POST['code'])) $code = $_POST['code'];
			else $code = null;
			echo json_encode($admin->evaluate($_POST['assignment_id'], 1, $code, $isAdmin));
		} else {
			$assignment_id = $_POST['assignment_id'];
			if(!$this->isAllowed($assignment_id)) {
				header('Location: /');
				return;
			}
			$judge = new Judge();
			echo json_encode($judge->evaluate($assignment_id));
		}
	}
	
	private function writeControlConfig($control) {
		
		$toWrite = '';
		foreach(array_keys($control) as $section) {
			$toWrite .= '['.$section.']';
			$toWrite .= "\n\n";
			foreach(array_keys($control[$section]) as $key) {
				if(is_array($control[$section][$key])) {
					foreach($control[$section][$key] as $entry) $toWrite .= $key.'[]' . '="' . $entry . '"' . "\n";
				} else {
					$toWrite .= $key . '="' . $control[$section][$key] . '"' . "\n";
				}
			}
			$toWrite .= "\n";
		}
			
		return file_put_contents(BASE_DIR . 'app/config/custom.ini', trim($toWrite));
	}
	
	private function isAllowed($assignment_id) {
	
		// TODO handle database connection failure
		try {
			$rows = Helper::cacheData("SELECT is_practice FROM problem WHERE id=(SELECT problem_id FROM assignment WHERE id=?)",
					array($assignment_id), 3600);
		} catch(Exception $e) {
			return true;
		}
	
		if(!count($rows)) return false;
		else {
			$isPractice = (intval($rows[0]['is_practice']) === 1);
			if($isPractice) return true;
			else {
				// TODO handle different event for assignment.
				$eventNow = Session::getEventNow();
				$stop = strtotime($eventNow['time_stop']);
				if(time() > $stop) return false;
				else return true;
			}
		}
	}
	
	private function checkAccess() {
		
		if(Session::getUserID()) {
			return true;
		} else {
			header("HTTP/1.1 403 Forbidden");
			die("You are not allowed to access this resource.");
		}
	}
	
}

?>
