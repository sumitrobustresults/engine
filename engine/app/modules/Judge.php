<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Judge {
	
	const RESULT_SUCCESS = "SUCCESS";
	const RESULT_FAILURE = "FAILURE";
	const VERDICT_ACCEPTED = "ACCEPTED";
	const VERDICT_WRONGANSWER = "WRONG_ANSWER";
	const VERDICT_TIMEDOUT = "TIMED_OUT";
	const VERDICT_ERROR = "ERROR";
	const EXECUTION_TYPE_TESTCASE = 0;
	const EXECUTION_TYPE_CUSTOM = 1;
	
	/**
	 * Evaluates the last compiled code on relevant
	 * test cases and outputs the result.
	 * 
	 * Tables used: testcase
	 */
	public function evaluate($assignment_id) {
		
		global $LOGGER;
		
		$delay = Registry::settings('DELAYS', 'evaluation');
		sleep(intval($delay / 1000));
		
		$user_id = Session::getUserID();
		
		$assignment = R::load('assignment', $assignment_id);
		$rows = R::getAssocRow("SELECT id,contents FROM code WHERE assignment_id=? ORDER BY save_time DESC LIMIT 1", array($assignment_id));
		$code = base64_decode($rows[0]['contents']);
		$code_id = $rows[0]['id'];
		
		$build_name = 'e' . $assignment->event_id . '_' . $assignment_id;
		
		$engine = Engine::instance();
		
		$compilation = $engine->compile($build_name, $code);
		if(!isset($compilation['executable']))
			return array(
				'verdict'=>$this::VERDICT_ERROR, 
				'results'=>array('status'=>'ER', 'output'=>''),
				'invisible'=>array('passed'=>0, 'total'=>0),
				'feedback'=>null
			); 
		$executable = $compilation['executable'];
		
		// Visible test cases.
		$query = "SELECT id AS test_id,input,output 
			FROM test_case
			WHERE problem_id=(SELECT problem_id FROM assignment WHERE id=:id)
			AND visibility=1";
		$testcases = Helper::cacheData($query, array(':id'=>$assignment_id));
		
		$evaluation = array();
		
		$results = array();
		$verdict = $this::VERDICT_ACCEPTED;
		
		$execType = Registry::lookupCustomConfig('type');
		
		foreach($testcases as $testcase) {
			$local_verdict = $this::VERDICT_ACCEPTED;
			if($execType === 'interpreted') {
				$actual = $engine->interpret($build_name, $code, $testcase['input']);
			} else {
				$actual = $engine->execute($executable, $testcase['input']);
			}
			if($actual === FALSE) {
				$result = array('status'=>'ER', 'output'=>'');
				$verdict = $this::VERDICT_ERROR;
				$results = array();
				// TODO log error results
				break;
			} else {
				$result = $actual;
				if(trim($testcase['output']) !== trim($result['output'])) {
					$verdict = $this::VERDICT_WRONGANSWER;
					$local_verdict = $this::VERDICT_WRONGANSWER;
				}
			}
			array_push($evaluation, array(
				'id'=>$testcase['test_id'],
				'output'=>$result['output'],
				'result'=>$result['status'],
				'verdict'=>$local_verdict
			));
			array_push($results, array(
				'id'=>$testcase['test_id'],
				'input'=>$testcase['input'],
				'expected'=>$testcase['output'],
				'actual'=>$result
			));
		}
		
		// Invisible test cases.
		$query = "SELECT id AS test_id,input,output 
			FROM test_case
			WHERE problem_id=(SELECT problem_id FROM assignment WHERE id=:id)
			AND visibility=0 AND type=1";
		$testcases = Helper::cacheData($query, array(':id'=>$assignment_id));
		
		$passed = 0;
		
		foreach($testcases as $testcase) {
			$local_verdict = $this::VERDICT_ACCEPTED;
			if($execType === 'interpreted') {
				$actual = $engine->interpret($build_name, $code, $testcase['input']);
			} else {
				$actual = $engine->execute($executable, $testcase['input']);
			}
			if($actual === FALSE) {
				$result = array('status'=>'ER', 'output'=>'');
				$verdict = $this::VERDICT_ERROR;
				// TODO log error results
				break;
			} else {
				$result = $actual;
				if(trim($testcase['output']) !== trim($result['output'])) {
					$verdict = $this::VERDICT_WRONGANSWER;
					$local_verdict = $this::VERDICT_WRONGANSWER;
				} else {
					$passed++;
				}
			}
			array_push($evaluation, array(
				'id'=>$testcase['test_id'],
				'output'=>$result['output'],
				'result'=>$result['status'],
				'verdict'=>$local_verdict
			));
		}
		
		Logger::instance()->logEvaluationResult($assignment_id, $code_id, $evaluation);
		
		$hooks = Registry::getHooks('on_evaluate');
		
		if(count($hooks)) {
			$feedback = array();
			foreach($hooks as $hook) {
				$output = Tool::instance()->invoke('on_evaluate', $hook, array(
					'problem'=>$assignment->problem_id,
					'code'=>$code
				));
				if($output !== null)
					array_push($feedback, $output['out']);
			}
			$feedback = array_filter($feedback);
		} else {
			$feedback = null;
		}
		
		if($verdict === $this::VERDICT_ACCEPTED) {
			$hooks = Registry::getHooks('on_accepted');
			if(count($hooks)) {
				foreach($hooks as $hook) {
					$output = Tool::instance()->invoke('on_accepted', $hook, array(
						'problem'=>$assignment->problem_id,
						'code'=>$code
					));
				}
			}
		}
		
		// Log activity
		/*R::exec("INSERT INTO activity (user_id,context,type,details) VALUES (:uid,'CODE','EVALUATE',:d)",
			array(':uid'=>Session::getUserID(), ':d'=>$build->compilation_id));*/
		
		return array(
			'verdict'=>$verdict, 
			'results'=>$results,
			'invisible'=>array('passed'=>$passed, 'total'=>count($testcases)),
			'feedback'=>$feedback
		);
	}
	
	/**
	 * Runs a custom test case on the last executable 
	 * generated for the corresponding problem (if any).
	 * Returns the output for the testcase.
	 */
	public function execute($assignment_id, $testcase) {
		
		$delay = Registry::settings('DELAYS', 'execution');
		sleep(intval($delay / 1000));
		
		$user_id = Session::getUserID();
		
		$assignment = R::load('assignment', $assignment_id);
		
		$rows = R::getAssocRow("SELECT id,contents FROM code WHERE assignment_id=? ORDER BY save_time DESC LIMIT 1", array($assignment_id));
		$code = base64_decode($rows[0]['contents']);
		$code_id = $rows[0]['id'];
		
		$build_name = 'e' . $assignment->event_id . '_' . $assignment_id;
		
		$compilation = Engine::instance()->compile($build_name, $code);
		if(!isset($compilation['executable']))
			return array('status'=>'ER', 'output'=>'');
		$executable = $compilation['executable'];
		
		$output = Engine::instance()->execute($executable, $testcase);
		
		if($output === FALSE) {
			// TODO handle error result
			return array('status'=>'ER', 'output'=>'');
		} else {
			/*R::exec("INSERT INTO activity (user_id,context,type,details) VALUES (:uid,'CODE','EXECUTE',:d)",
				array(':uid'=>Session::getUserID(), ':d'=>$build->compilation_id));*/
			Logger::instance()->logExecutionResult($assignment_id, $output['status'], $testcase, $output['output']);
			return $output;
		}
	}
	
	public function interpret($assignment_id, $code, $input) {
		
		$delay = Registry::settings('DELAYS', 'execution');
		sleep(intval($delay / 1000));
		
		$user_id = Session::getUserID();
		
		$assignment = R::load('assignment', $assignment_id);
		
		$build_name = 'e' . $assignment->event_id . '_' . $assignment_id;
		
		$output = Engine::instance()->interpret($build_name, base64_decode($code), $input);
		
		if($output === FALSE) {
			// TODO handle error result
			return array('status'=>'ER');
		} else {
			/*R::exec("INSERT INTO activity (user_id,context,type,details) VALUES (:uid,'CODE','EXECUTE',:d)",
			 array(':uid'=>Session::getUserID(), ':d'=>$build->compilation_id));*/
			Logger::instance()->logExecutionResult($assignment_id, $output['status'], $input, $output['output']);
			return $output;
		}
	}
	
	/**
	 * Compiles a code, logs it and returns the result.
	 * 
	 * Tables used: compilations
	 */
	public function compile($assignment_id, $code) {
		
		$delay = Registry::settings('DELAYS', 'compilation');
		sleep(intval($delay / 1000));
		
		$assignment = R::load('assignment', $assignment_id);
		
		$build_name = 'e' . $assignment->event_id . '_' . $assignment_id;
		
		$result = Engine::instance()->compile($build_name, $code);
		
		$code_id = $this->saveCode($assignment_id, $code);
		$compilation_id = Logger::instance()->logCompilationResult($assignment_id, $code_id, $result['raw'], ($result['result'] === 'success'));
		
		$result['code_id'] = $code_id;
		
		/*R::exec("INSERT INTO activity (user_id,context,type,details) VALUES (:uid,'CODE','COMPILE',:d)",
			array(':uid'=>Session::getUserID(), ':d'=>$compilation_id));*/
		
		return $result;
	}
	
	private function saveCode($assignment_id, $source) {
		
		$code = R::dispense('code');
		
		$code->assignment_id = $assignment_id;
		$code->user_id = Session::getUserID();
		$code->contents = base64_encode($source);
		$code->save_type = 3;
		
		return R::store($code);
	}
}

?>