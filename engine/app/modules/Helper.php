<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

use Predis\Client as PredisClient;

class Helper {
	
	private static $memcached = null;
	private static $redis = null;
	
	/*
	 * Required environment variables:
	 * 1. CACHE_TYPE
	 * 2. CACHE_ADDR
	 * 3. CACHE_PORT
	 */
	public static function cacheData($query, $params, $ttl = 1800) {
		
		$TYPE = $_SERVER['CACHE_TYPE'];
		$HOST = $_SERVER['CACHE_ADDR'];
		$PORT = $_SERVER['CACHE_PORT'];
		
		$hosts = json_decode($HOST, true);
		if($hosts && is_array($hosts)) {
			$HOST = $hosts;
		}
		
		// Create a hash to be referenced in future.
		$hash = md5( $query . json_encode($params) );
		
		// TODO Clustering support.
		if($TYPE === 'REDIS') {
			// Create an instance if not created.
			if(self::$redis === null) {
				self::$redis = new PredisClient();
			}
			// Check and fetch rows from query.
			if(self::$redis->exists($hash)) {
				$value = self::$redis->get($hash);
				return json_decode($value, true);
			} else {
				$rows = R::getAssocRow($query, $params);
				self::$redis->set($hash, json_encode($rows));
				self::$redis->expire($hash, $ttl);
				return $rows;
			}
		} else {
			// Create an instance if not created.
			if(self::$memcached === null) {
				self::$memcached = new Memcached();
				// Servers
				$servers = array();
				if(is_array($HOST)) {
					foreach($HOST as $h)
						array_push($servers, array($h, intval($PORT)));
				} else
					array_push($servers, array($HOST, intval($PORT)));
				self::$memcached->addServers($servers);
				self::$memcached->setOptions(array(
					Memcached::OPT_HASH => Memcached::HASH_MD5,
					Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
					Memcached::OPT_LIBKETAMA_COMPATIBLE => true
				));
			}
			// Check and fetch rows from query.
			if(($value = self::$memcached->get($hash)) !== false) {
				return json_decode($value, true);
			} else {
				$rows = R::getAssocRow($query, $params);
				self::$memcached->set($hash, json_encode($rows), time() + $ttl);
				return $rows;
			}
		}
	}
	
}
?>