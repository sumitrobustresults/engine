<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Analytics {
	
	private $CHUNK_SIZE = 1000;
	
	public static function instance() {
		return new self();
	}
	
	function tokenizeSource($source) {
		
		$filename = md5(time() . "" . rand()) . ".c";
		$source_file = BASE_DIR . PATH_APPDATA . $filename;
		$tool_path = BASE_DIR . PATH_THIRD_PARTY . "Lexer";
		
		if(!file_exists(PATH_APPDATA . 'Lexer')) {
			copy($tool_path, PATH_APPDATA . 'Lexer');
			chmod(PATH_APPDATA.'Lexer', 0777);
		}
		
		file_put_contents($source_file, $source);
		
		chdir(PATH_APPDATA);
		
		$cmd = "./Lexer -c $filename 2>&1";
		
		$cmdout = shell_exec($cmd);
		$csv = array_slice(array_filter(explode("\n", $cmdout)), 1);
		
		chdir(BASE_DIR);
		
		$source = '';
		
		$count = 0;
		$offset = 0;
		$tokens = array();
		$ids = array();
		$map = array();
		
		foreach($csv as $line) {
			$count++;
			$set = explode(",", $line);
			array_push($tokens, $set);
			$snippet = base64_decode($set[3]);
			$id = substr($set[0], 0, strpos($set[0], '('));
			array_push($ids, $id);
			if($id === 'EOF_GOOD')
				continue;
			if($id === 'PREINCLUDE')
				$source = $source . "/*$id*/ $snippet ";
			else 
				$source = $source . "/*$id*/ $snippet \n";
		}
		
		if(file_exists($source_file))
			unlink($source_file);
		
		$stream = array();
		$count = 0;
		foreach($ids as $id) {
			array_push($stream, "#$count#$id");
			$count++;
		}
		
		return array(
			'cmdout'=>$cmdout,
			'source'=>base64_encode($source),
			'tokens'=>$tokens,
			'ids'=>$ids,
			'stream'=>implode(" ", $stream)
		);
	}
	
	public function hashErrors() {
		
		$reg_quotes = "/'([a-zA-Z0-9 ;:_,`=#@\$\|\!\"<>%\{\}&\.\/\+\-\(\)\^\*\[\]\\\]+)'|\"([a-zA-Z0-9 _\+\-\*\/]+)\"/";
		
		do {
			$processed = array();
			$rows = R::getAssocRow("SELECT id,code_id,type,message,line FROM compilation_error WHERE id NOT IN (SELECT error_id FROM ext_syntax)
					 ORDER BY id LIMIT " . $this->CHUNK_SIZE);
			foreach($rows as $row) {
				$error = $row['message'];
				preg_match($reg_quotes, $error, $matches);
				if(count($matches)) {
					$set = array();
					$index = 1;
					do {
						array_push($set, $matches[1]);
						$error = str_replace($matches[0], ':X' . $index, $error);
						preg_match($reg_quotes, $error, $matches);
						$index++;
					} while(count($matches));
				}
				$hash = md5($error);
				$query = sprintf("INSERT INTO ext_syntax (code_id,type,error_id,error,hash,line,message) VALUES ('%s','%s','%s','%s','%s','%s','%s')", 
					$row['code_id'], $row['type'], $row['id'], addslashes($error), $hash, $row['line'], addslashes($row['message']));
				array_push($processed, $query);
			}
			if(count($processed)) R::exec(implode(";", $processed));
		} while(count($rows));
		
	}

	public function computeScores() {
		
		do {
			$processed = array();
			$rows = R::getAssocRow("SELECT id,user_id,problem_id,event_id FROM assignment WHERE id NOT IN (SELECT assignment_id FROM score)
					 ORDER BY id LIMIT " . $this->CHUNK_SIZE);
			foreach($rows as $row) {
				$score = $this->calculateAssignmentScore($row['id']);
				$query = sprintf("INSERT INTO score (user_id,event_id,assignment_id,problem_id,score) VALUES ('%s','%s','%s','%s','%s')",
					$row['user_id'], $row['event_id'],$row['id'],$row['problem_id'], $score);
				array_push($processed, $query);
			}
			if(count($processed)) R::exec(implode(";", $processed));
			$this->updateStatistics();
			echo "processed: " . count($rows) . ".\n";
		} while(count($rows));
	} 
	
	public function updateStatistics() {
		$query = "UPDATE score SET mean=(SELECT mean FROM stats WHERE stats.problem_id=score.problem_id),
				sd=(SELECT sd FROM stats WHERE stats.problem_id=score.problem_id)";
		R::exec($query);
		$query = "UPDATE score SET relative=(score-mean)";
		R::exec($query);
	}
	
	public function calculateAssignmentScore($assignment_id) {
		
		// Get the time coding started.
		$query = "SELECT time_start,time_stop FROM schedule WHERE event_id=(SELECT event_id FROM assignment WHERE id=?) AND id IN (SELECT schedule_id FROM slot WHERE section=
		(SELECT section FROM account WHERE id=(SELECT user_id FROM assignment WHERE id=?)))";
		$rows = R::getAssocRow($query, array($assignment_id, $assignment_id));
		
		if(count($rows)) $times = $rows[0];
		else $times = null;
		
		if($times == null) return null;
		
		// Get the time till the first evaluation success
		$query = "SELECT compilation_id,evaluate_time FROM evaluation AS main WHERE
		assignment_id=? AND
		NOT EXISTS (SELECT id FROM evaluation AS alt WHERE alt.compilation_id=main.compilation_id AND alt.verdict='WRONG_ANSWER') AND
		evaluate_time=(SELECT MIN(evaluate_time) FROM evaluation AS main WHERE
			assignment_id=? AND
			NOT EXISTS (SELECT id FROM evaluation AS alt WHERE alt.compilation_id=main.compilation_id AND alt.verdict='WRONG_ANSWER'))";
		$rows = R::getAssocRow($query, array($assignment_id, $assignment_id));
		
		if(count($rows)) {
			$evaluation_success = $rows[0]['evaluate_time'];
			$compile_id = $rows[0]['compilation_id'];
		} else {
			$evaluation_success = null;
			$compile_id = null;
		}
		
		if($evaluation_success == null) {
			// Count code versions.
			$query = "SELECT COUNT(id) AS count FROM code WHERE assignment_id=?";
			$rows = R::getAssocRow($query, array($assignment_id));
			$count = $rows[0]['count'];
			if($count <= 1) return null;
			// Set to event end.
			$evaluation_success = $times['time_stop'];
			$query = "SELECT compilation_id FROM evaluation WHERE assignment_id=? ORDER BY evaluate_time DESC LIMIT 1";
			$rows = R::getAssocRow($query, array($assignment_id));
			if(count($rows)) $compile_id = $rows[0]['compilation_id'];
		}
		
		$start = strtotime($times['time_start']);
		$stop = strtotime($evaluation_success);
		
		// The elapsed time to success.
		$ELAPSED = intval(($stop - $start) / 60);
		
		// Get number of compilation successes.
		$query = "SELECT COUNT(id) AS success FROM compilation WHERE assignment_id=? AND compiled=1";
		$rows = R::getAssocRow($query, array($assignment_id));
		
		if(count($rows)) $success = $rows[0]['success'];
		else $success = 0;
		
		// Get number of compilation failures.
		$query = "SELECT COUNT(id) AS failure FROM compilation WHERE assignment_id=? AND compiled=0";
		$rows = R::getAssocRow($query, array($assignment_id));
		
		if(count($rows)) $failure = $rows[0]['failure'];
		else $failure = 0;
		
		$total = $success + $failure;
		if($total == 0) $COMPILE = 0;
		else $COMPILE = intval(($failure * 100) / $total);
		
		// Get the number of failed evaluation attempts till success.
		$query = "SELECT COUNT(DISTINCT compilation_id) AS attempts FROM evaluation WHERE assignment_id=? AND evaluate_time < ?";
		$rows = R::getAssocRow($query, array($assignment_id, $evaluation_success));
		
		$ATTEMPTS = $rows[0]['attempts'];
		
		// Get the number of failed test cases.
		if($compile_id === null) {
			$FAILED = 100;
		} else {
			$query = "SELECT COUNT(id) AS count FROM evaluation WHERE compilation_id=? AND verdict='WRONG_ANSWER'";
			$rows = R::getAssocRow($query, array($compile_id));
			$failed = $rows[0]['count'];
			$query = "SELECT COUNT(id) AS count FROM evaluation WHERE compilation_id=?";
			$rows = R::getAssocRow($query, array($compile_id));
			$total = $rows[0]['count'];
			$FAILED = intval($failed * 100 / $total);
		}
		
		$W = array( 1, 1, 10, 1);
		
		$SCORE = ($W[0] * $ELAPSED) + ($W[1] * $COMPILE) + ($W[2] * $ATTEMPTS) + ($W[3] * $FAILED);
		
		return $SCORE;
	}
	
	public function getErrorHash($error) {
		
		$reg_quotes = "/'([a-zA-Z0-9 ;:_,`=#@\$\|\!\"<>%\{\}&\.\/\+\-\(\)\^\*\[\]\\\]+)'|\"([a-zA-Z0-9 _\+\-\*\/]+)\"/";
		$vars = array();
		
		preg_match($reg_quotes, $error, $matches);
		
		if(count($matches)) {
			$set = array();
			$index = 1;
			do {
				array_push($set, $matches[1]);
				$error = str_replace($matches[0], ':X' . $index, $error);
				$vars[':X' . $index] = substr($matches[0], 1, strlen($matches[0]) - 2);
				preg_match($reg_quotes, $error, $matches);
				$index++;
			} while(count($matches));
		}
		
		$hash = md5($error);
		
		return array(
			'hash'=>$hash,
			'vars'=>$vars
		);
	}
	
}

?>