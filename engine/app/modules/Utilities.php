<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Utilities {
	
	public static function convertToCapitalCase($text) {
		
		$parts = array_filter(explode(' ', $text));
		
		$convertedPieces = array();
		
		foreach($parts as $part) {
			$part = trim($part);
			$converted = strtoupper(substr($part, 0, 1)).strtolower(substr($part, 1));
			array_push($convertedPieces, $converted);
		}
		
		return implode(' ', $convertedPieces);
	}
	
	public static function shortenText($text, $maxLength) {
		
		if(strlen($text) <= $maxLength) return $text;
		
		$snip = strip_tags(substr($text, 0, $maxLength), '<b><br>');
		
		if(strlen($snip) < $maxLength) $snip = substr($text, 0, 2*$maxLength - strlen($snip)).'...';
		
		return $snip;
	}
	
	public static function convertToFullDate($timestamp) {
		
		return sprintf('%s, at %s', date('l jS F Y', $timestamp), date('g:i A', $timestamp));
		
	}
	
	public static function getFormatForTimeago($timestamp) {
		
		return date('dmYGis', $timestamp);
		
	}
	
	public static function printBooleanResultJSON($result) {
		
		if($result) echo json_encode(TRUE);
		else echo json_encode(FALSE);
	}
	
}

?>