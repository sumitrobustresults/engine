<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Debugger {
	
	private $executable = null;
	private $std_input = null;
	
	private $debugger = null;
	
	private $stdin = null;
	private $stdout = null;
	private $stderr = null;
	
	function __construct($executable, $std_input = null) {
		$this->executable = $executable;
		$this->std_input = $std_input;
	}
	
	// Initializes the debugger and starts a debug session.
	public function debug_init() {
		
		chdir(PATH_APPDATA);
		
		$descriptors = array(
			0 => array('pipe', 'r'),  // stdin
			1 => array('pipe', 'w'),  // stdout
		    2 => array('pipe', 'w')   // stderr
	    );
		
		$command = sprintf("exec gdb %s -q --interpreter=mi2", $this->executable);
		
		$this->debugger = proc_open($command, $descriptors, $pipes);
		
		if(!is_resource($this->debugger)) return FALSE;
		
		$this->stdin = $pipes[0];
		$this->stdout = $pipes[1];
		$this->stderr = $pipes[2];
		
		$result = stream_set_blocking($this->stdout, 0);
		
		if(!$result) return FALSE;
		
		stream_set_timeout($this->stdout, 5);
		
		// For the initial gdb startup messages
		$this->read_next();
		
		chdir(BASE_DIR);
		
		return TRUE;
	}
	
	// Exits the debugger.
	public function debug_stop() {
		
		fwrite($this->stdin, "-gdb-exit\n");
		
		fclose($this->stdin);
		fclose($this->stdout);
		fclose($this->stderr);
		
		$probe_time = time();
		do {
			$status = proc_get_status($this->debugger);
			if(time() - $probe_time > 3 || !$status['running']) break;
			sleep(1);
		} while(TRUE);
		
		proc_terminate($this->debugger, 9);
		proc_close($this->debugger);
	}
	
	// Runs an arbitrary command on the debugger.
	public function run_command($command) {
		
		if(!is_resource($this->debugger)) return FALSE;
		
		fwrite($this->stdin, $command . "\n");
		
		$output = $this->read_next();
		
		if(preg_match('/[\^\*]running/', $output[count($output) - 1])) $output = array_merge($output, $this->read_next());
		
		return $output;
	}
	
	// Reads the next set of lines till the cursor.
	private function read_next() {
		
		$output = '';
		
		while(TRUE) {
			$content = stream_get_contents($this->stdout);
			$output .= $content;
			if(strpos($content, "(gdb)") !== FALSE) break;
		}
		
		//$GLOBALS['LOGGER']->logDebug($output);
		
		$output = trim(str_replace("(gdb)", "", $output));
		
		return explode("\n", $output);
	}
	
}

?>