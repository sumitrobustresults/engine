<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Admin {
	
	public static function instance() {
		return new self();
	}
	
	public function compile($code, $admin = true) {
		
		$build_name = Session::getUserID();
		
		if(!$admin) {
			$rows = R::getAssocRow("SELECT value AS delay FROM configuration WHERE setting='compilation'");
			$delay = json_decode($rows[0]['delay'], true);
			sleep(intval($delay / 1000));
		}
		
		$output = Engine::instance()->compile($build_name, $code);
		
		return $output;
	}
	
	public function execute($code, $testcase, $admin = true) {
		
		$ext_exe = Registry::lookupCustomConfig('extension_executable');
		$executable = Session::getUserID() . '.' . $ext_exe;
		
		$build_name = Session::getUserID();
		
		if(!$admin) {
			$rows = R::getAssocRow("SELECT value AS delay FROM configuration WHERE setting='execution'");
			$delay = json_decode($rows[0]['delay'], true);
			sleep(intval($delay / 1000));
		}
		
		$output = Engine::instance()->compile($build_name, base64_decode($code));
		if($output['result'] === 'success')
			$output = Engine::instance()->execute($output['executable'], $testcase);
		else $output = null;
		
		return $output;
	}
	
	public function interpret($code, $input, $admin = true) {
		
		$build_name = Session::getUserID();
		
		if(!$admin) {
			$rows = R::getAssocRow("SELECT value AS delay FROM configuration WHERE setting='compilation'");
			$delay = json_decode($rows[0]['delay'], true);
			sleep(intval($delay / 1000));
		}
		
		$output = Engine::instance()->interpret($build_name, base64_decode($code), $input);
		
		return $output;
	}
	
	public function evaluate($assignment_id, $type = 1, $code = null, $admin = true) {
		
		global $LOGGER;
		
		if(!$admin) {
			$rows = R::getAssocRow("SELECT value AS delay FROM configuration WHERE setting='evaluation'");
			$delay = json_decode($rows[0]['delay'], true);
			sleep(intval($delay / 1000));
		}
		
		$defaulter = false;
		$query = "SELECT problem_id,is_submitted,(SELECT CASE WHEN is_submitted=1 THEN 
				(SELECT contents FROM code WHERE code.id=submission) ELSE 
				(SELECT contents FROM code WHERE assignment_id=assignment.id ORDER BY save_time DESC LIMIT 1) END) AS code 
				FROM assignment WHERE id=:id";
		$rows = R::getAssocRow($query, array(':id'=>$assignment_id));
		
		$execType = Registry::lookupCustomConfig('type');
		
		if(count($rows)) {
			$assignment = $rows[0];
			$query = "SELECT id,type,visibility,input,output FROM test_case WHERE problem_id=:id AND type>=:type";
			$testCases = R::getAssocRow($query, array(':id'=>$assignment['problem_id'], ':type'=>$type));
			if(!count($testCases)) {
				return false;
			}
			$ext_src = Registry::lookupCustomConfig('extension_source');
			$ext_exe = Registry::lookupCustomConfig('extension_executable');
			$build_name = null;
			$file_source = null;
			while(!$file_source) {
				$build_name = 'admin_' . generate_random_string();
				$file_source = sprintf('%s.%s', $build_name, $ext_src);
				if(file_exists(PATH_APPDATA.$file_source)) $file_source = null;
			}
			if($execType === 'compiled') {
				// Compile
				if($code === null) $code = $assignment['code'];
				$output = Engine::instance()->compile($build_name, base64_decode($code));
				if(file_exists(PATH_APPDATA.$file_source)) unlink(PATH_APPDATA.$file_source);
				if($output['result'] !== 'success') { return array('compilation'=>$output); }
				$executable = $build_name . '.' . $ext_exe;
			} else {
				$output = null;
			}
			// Evaluate
			$results = array();
			$verdict = Judge::VERDICT_ACCEPTED;
			$engine = Engine::instance();
			foreach($testCases as $test) {
				if($execType === 'interpreted') {
					$actual = $engine->interpret($build_name, base64_decode($code), $test['input']);
				} else {
					$actual = $engine->execute($executable, $test['input']);
				}
				if($actual === FALSE) {
					$result = array('status'=>'ER');
					$verdict = Judge::VERDICT_ERROR;
					$results = array();
					// TODO log error results
					break;
				} else {
					$result = $actual;
					if(trim($test['output']) !== trim($result['output'])) $verdict = Judge::VERDICT_WRONGANSWER;
				}
				array_push($results, array(
				'id'=>$test['id'],
				'type'=>$test['type'],
				'visibility'=>$test['visibility'],
				'input'=>$test['input'],
				'expected'=>$test['output'],
				'actual'=>$result
				));
			}
			if(isset($executable) && file_exists(PATH_APPDATA.$executable)) unlink(PATH_APPDATA.$executable);
			return array(
					'compilation'=>$output,
					'evaluation'=>$results,
					'verdict'=>$verdict,
					'defaulter'=>(intval($assignment['is_submitted']) === 0)
			);
		} else {
			return null;
		}
	}
	
}

?>
