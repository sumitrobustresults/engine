<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Engine {
	
	const VERDICT_ACCEPTED = "ACCEPTED";
	const VERDICT_WRONGANSWER = "WRONG_ANSWER";
	const VERDICT_TIMEDOUT = "TIMED_OUT";
	const VERDICT_ERROR = "ERROR";
	
	private $STATUS_KNOWN = array('OK', 'RT', 'RF', 'TL', 'AT', 'ML');
	private $STATUS_UNKNOWN = array('PD', 'OL', 'IE', 'BP');
	
	/**
	 * Returns an instance of this.
	 * 
	 * @return Engine
	 */
	public static function instance() {
		return new self();
	} 
	
	/**
	 * Compiles the code and returns the result. The build name is the 
	 * name of the unique identifier for a code version. The source
	 * file is not deleted after compilation and the executable is kept.
	 * The name of the executable is <build_name>.out.
	 * 
	 * @param string $build_name
	 * @param string $code
	 * @return array
	 */
	public function compile($build_name, $code) {
		
		$hooks = Registry::getHooks('pre_compile');
		
		if(count($hooks)) {
			$checks = array();
			foreach($hooks as $hook) {
				$res = Tool::instance()->invoke('pre_compile', $hook, array(
					'code'=>base64_encode($code)
				));
				if($res)
					$checks = array_merge($checks, $res);
			}
		} else {
			$checks = null;
		}
		
		$flags = Registry::settings('COMPILER_FLAGS', 'flag');
		
		$ext_src = Registry::lookupCustomConfig('extension_source');
		$ext_exe = Registry::lookupCustomConfig('extension_executable');
		
		$file_source = sprintf("%s.%s", $build_name, $ext_src);
	 
		$file_executable = sprintf("%s.%s", $build_name, $ext_exe);
	
		file_put_contents(PATH_APPDATA.$file_source, $code);
	
		chdir(BASE_DIR.PATH_APPDATA);
		
		$command = Registry::lookupCustomConfig('command_compilation');
		$command = str_replace('<input_file>', $file_source, $command);
		$command = str_replace('<output_file>', $file_executable, $command);
		$flags = " " . implode(" ", $flags);
		
		$cmd = $command . $flags;
		
		$descriptors = array(
			0 => array('pipe', 'r'),  // stdin
			1 => array('pipe', 'w'),  // stdout
			2 => array('pipe', 'w')   // stderr
		);
		
		$process = proc_open('exec ' . $cmd, $descriptors, $pipes);
		
		if(!is_resource($process)) {
			chdir(BASE_DIR);
			return FALSE;
		}
		
		$buffer = stream_get_contents($pipes[1]);
		$errors = trim(stream_get_contents($pipes[2]));
		
		fclose($pipes[0]);
		fclose($pipes[1]);
		fclose($pipes[2]);
		
		proc_close($process);
		
		if($errors && strpos($errors, 'error:') !== FALSE) {
			$compiled = false;
		} else {
			$compiled = true;
		}
		
		$output = $errors;
	
		chdir(BASE_DIR);
		
		$hooks = Registry::getHooks('on_compile');
		
		if(count($hooks)) {
			$feedback = array();
			foreach($hooks as $hook) {
				$res = Tool::instance()->invoke('on_compile', $hook, array(
					'code'=>$code,
					'output'=>$output
				));
				if($res !== null)
					$feedback = array_merge($feedback, $res);
			}
		} else {
			$feedback = null;
		}
		
		if($compiled) {
			$result = array('result'=>'success', 'executable'=>$file_executable, 'messages'=>$feedback, 'raw'=>$output, 'checks'=>$checks);
		} else {
			$result = array('result'=>'failure', 'messages'=>$feedback, 'raw'=>$output, 'checks'=>$checks);
		}
		
		return $result;
	}
	
	/**
	 * Executes a pre-compiled file and returns the results. The execu-
	 * table file is not deleted and is kept for future re-use. The 
	 * testcase is the input to the program via stdin. Runtime analysis
	 * is also carried out.
	 * 
	 * @param string $executable
	 * @param string $testcase
	 * @return boolean|array
	 */
	public function execute($executable, $testcase, $runtimeAnalysis = FALSE) {
		
		global $LOGGER;
		
		if(!file_exists(PATH_APPDATA.$executable)) {
			return FALSE;
		}
		
		$quota_time = Registry::settings('EXECUTION_QUOTAS', 'time');
		$quota_memory = Registry::settings('EXECUTION_QUOTAS', 'memory');

		// Permission problems?
		//chmod(PATH_APPDATA.$executable, 0777);
	
		if(!file_exists(PATH_APPDATA.'sandbox')) {
			copy(PATH_THIRD_PARTY.'sandbox', PATH_APPDATA.'sandbox');
			chmod(PATH_APPDATA.'sandbox', 0777);
		}
	
		chdir(PATH_APPDATA);
		
		$command = Registry::lookupCustomConfig('command_execution');
		$command = str_replace('<output_file>', $executable, $command);
		$cmd = sprintf("./sandbox %s %s %s", $command, $quota_time, $quota_memory);
		
		$descriptors = array(
			0 => array('pipe', 'r'),  // stdin
			1 => array('pipe', 'w'),  // stdout
			2 => array('pipe', 'w')   // stderr
		);
	
		$process = proc_open('exec ' . $cmd, $descriptors, $pipes);
	
		if(!is_resource($process)) {
			chdir(BASE_DIR);
			return FALSE;
		}
	
		fwrite($pipes[0], $testcase);
		fclose($pipes[0]);
	
		$buffer = stream_get_contents($pipes[1]);
		$errors = stream_get_contents($pipes[2]);
	
		fclose($pipes[1]);
		fclose($pipes[2]);
	
		proc_close($process);
	
		chdir(BASE_DIR);
			
		if(empty($errors)) {
			$output = FALSE;
		} else {
			$errors = trim($errors);
			if(preg_match("/([a-zA-Z]+):([0-9]+):([0-9]+)/", $errors)) {
				$results = explode(':', $errors);
				$status = strtoupper($results[0]);
				$cpu = $results[1];
				$mem = $results[2];
				if(in_array($status, $this->STATUS_KNOWN)) {
					if($status === 'OK') {
						$feedback = null;
					} else {
						//$buffer = '';
						if($runtimeAnalysis) 
							$feedback = Feedback::instance()->getExecutionFeedback($status, $executable, $testcase);
						else $feedback = null;
					}
					$buffer = preg_replace('/[\x00-\x08\x0B-\x1F\x7F-\xFF]/', '', $buffer);
					$output = array('output'=>$buffer, 'status'=>$status, 'errors'=>$errors, 'feedback'=>$feedback);
				} else {
					file_put_contents(BASE_DIR.'data/exec-errors.log', $errors."\n", FILE_APPEND);
					$output = FALSE;
				}
			} else {
				file_put_contents(BASE_DIR.'data/exec-errors.log', $errors."\n", FILE_APPEND);
				$output = FALSE;
			}
		}
	
		if($output === FALSE) {
			// TODO handle error result
			return array('status'=>'ER', 'output'=>'');
		} else {
			return $output;
		}
	}
	
	public function interpret($build_name, $code, $input) {
		
		$quota_time = Registry::settings('EXECUTION_QUOTAS', 'time');
		$quota_memory = Registry::settings('EXECUTION_QUOTAS', 'memory');
		
		/*if(!file_exists(PATH_APPDATA.'sandbox')) {
			copy(PATH_THIRD_PARTY.'sandbox', PATH_APPDATA.'sandbox');
			chmod(PATH_APPDATA.'sandbox', 0777);
		}*/
		
		$ext_src = Registry::lookupCustomConfig('extension_source');
		
		$file_source = sprintf("%s.%s", $build_name, $ext_src);
		file_put_contents(PATH_APPDATA.$file_source, $code);
		chmod(PATH_APPDATA . $file_source, 0777);
		
		chdir(BASE_DIR.PATH_APPDATA);
		
		$command = Registry::lookupCustomConfig('command_execution');
		$command = str_replace('<input_file>', $file_source, $command);
		
		$descriptors = array(
				0 => array('pipe', 'r'),  // stdin
				1 => array('pipe', 'w'),  // stdout
				2 => array('pipe', 'w')   // stderr
		);
		
		$process = proc_open('exec ' . $command, $descriptors, $pipes);
		
		if(!is_resource($process)) {
			chdir(BASE_DIR);
			return FALSE;
		}
		
		fwrite($pipes[0], $input);
		fclose($pipes[0]);
		
		$buffer = stream_get_contents($pipes[1]);
		$errors = trim(stream_get_contents($pipes[2]));
		
		fclose($pipes[1]);
		fclose($pipes[2]);
		
		proc_close($process);
		
		chdir(BASE_DIR);
		
		if(empty($errors))
			$output = array('output'=>$buffer, 'status'=>'OK', 'errors'=>$errors, 'feedback'=>null);
		else 
			$output = array('output'=>$buffer, 'status'=>'CT', 'errors'=>$errors, 'feedback'=>null);
		
		if($output === FALSE) {
			// TODO handle error result
			return array('status'=>'ER');
		} else {
			return $output;
		}
	}
}

?>