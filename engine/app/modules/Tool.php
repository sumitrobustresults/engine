<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Tool {
	
	public static function instance() {
		return new self();
	}
	
	/**
	 * Sends an URL request with parameters.
	 * 
	 * @param string $url
	 * @param mixed $params
	 * @return string
	 */
	public function invoke($event, $hook, $params) {
		
		$tools = array();
		$enabled = false;
		
		$rows = R::getAssocRow("SELECT section,value FROM configuration WHERE setting=?", array($hook[0]));
		
		foreach($rows as $row) {
			if($row['section'] === 'TOOLS')
				$enabled = json_decode($row['value'], true);
			else
				$addr = json_decode($row['value'], true);
		}
		
		if(!$enabled)
			return null;
		
		$loc = $addr[round(rand() * (count($addr) - 1))];
		$url = $hook[1];
		
		$url = "http://" . $loc . $url;
		
		$c = curl_init();
		
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_POST, true);
		curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($params));
		
		$response = curl_exec($c);
		
		return json_decode($response, true);
	}
	
	private function run($command, $args = array()) {
		
		$descriptors = array(
			0 => array('pipe', 'r'),  // stdin
			1 => array('pipe', 'w'),  // stdout
			2 => array('pipe', 'w')   // stderr
		);
		
		$command = sprintf("exec %s %s", $command, implode(' ', $args));
		
		$process = proc_open($command, $descriptors, $pipes);
		
		if(!is_resource($process)) return FALSE;
		
		fclose($pipes[0]);
		
		$stdout = stream_get_contents($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		
		fclose($pipes[1]);
		fclose($pipes[2]);
		
		proc_close($process);
		
		return array(
			'output'=>$stdout,
			'errors'=>$stderr
		);
	}
	
	private function log($toolID, $codeID, $output, $errors) {
		
		$feedback = R::dispense('feedback');
		
		$feedback->tool_id = $toolID;
		$feedback->code_id = $codeID;
		$feedback->output = $output;
		$feedback->errors = $errors;
		
		return R::store($feedback);
	}
	
}

?>