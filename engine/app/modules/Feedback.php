<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Feedback {
	
	/*private $compilation = array(
		"/is used uninitialized/"									=> 'UNINITIALIZED_VARIABLE',
		"/implicit declaration of function/" 						=> 'HEADER_INCLUDE',
		"/unused variable/" 										=> 'UNUSED_VARIABLE',
		"/control reaches end of non-void function/" 				=> 'NO_RETURN',
		"/expected declaration or statement at end of input/" 		=> 'UNCLOSED_BRACES',
		"/expects argument of type/" 								=> 'ARGUMENT_MISMATCH',
		"/expected ';' before/" 									=> 'MISSING_SEMICOLON',
		"/undeclared \(first use in this function\)/" 				=> 'UNDECLARED_VARIABLE',
		"/each undeclared identifier is reported only once/"		=> 'UNDECLARED_VARIABLE',
		"/'else' without a previous 'if'/"							=> 'IMPROPER_CONDITIONAL',
		"/statement with no effect/"								=> 'USELESS_STATEMENT',
		"/expected identifier or '\(' before ('{'|'}') token/"		=> 'INVALID_SCOPE',
		"/lvalue required as left operand of assignment/"			=> 'INVALID_LVALUE',
		"/format '%[a-z]+' expects a matching '[a-z]+' argument/"	=> 'INCORRECT_IO',
		"/expected '}' before 'else'/"								=> 'UNCLOSED_IF_BRACE'
	);
	
	private $compileErrorMessages = array(
		'UNINITIALIZED_VARIABLE'=> 'You have variables over here that are not initialized. Are you doing this purposely? An uninitialized variable contains garbage value.',
		'HEADER_INCLUDE' 		=> 'Seems that you have missed out some "#include <header_file.h>" statements at the top of your code. How about "string.h" or "math.h" perhaps?',
		'NO_RETURN' 			=> 'Your method has a return type but does not return any value. Add a return statement at the end of the method or wherever necessary.',
		'UNCLOSED_BRACES' 		=> 'Well this should not happen with this editor but it seems that you have not closed a few braces "{}". Check the braces in your code and make sure they are closed.',
		'MISSING_SEMICOLON' 	=> 'Whoops! Missed a semicolon in the previous line(s) it seems. Add a semicolon after each statement.',
		'UNUSED_VARIABLE' 		=> 'You have unused variables here. You might consider removing them.',
		'ARGUMENT_MISMATCH' 	=> 'Your argument type and data type does not match here. Change either of them.',
		'UNDECLARED_VARIABLE' 	=> 'You have an undeclared variable here. You should first declare the variables before you can use them. (eg: int myVariable;)',
		'IMPROPER_CONDITIONAL'	=> 'You seem to be writing conditionals incorrectly. An "else" should be preceded by an "if".',
		'USELESS_STATEMENT'		=> 'Something is not right here. This statement practically has no effect on your program. You might consider checking the operators.',
		'INVALID_SCOPE'			=> 'Are you trying to define a function over here? This block of code is invalid. Recollect how scopes are defined.',
		'INVALID_LVALUE'		=> 'The operand on the left does not seem to be assignable. Are you assigning a value to a non-variable?',
		'INCORRECT_IO'			=> 'You seem to be using printf/scanf statements incorrectly. The format specifiers in the string must match a corresponding variable in the arguments list. (eg: printf("%d", x) [Here x is the variable corresponding to %d])',
		'UNCLOSED_IF_BRACE'		=> 'It seems that you have not closed the brace "}" for the preceding "if" statement. Please check the braces.'
	);*/
	
	public static function instance() {
		return new self();
	}
	
	public function getCompilerFeedback($output) {
		
		$lines = array_filter(explode("\n", $output));
		
		$errors = array();
		
		/**
		 * Each valid message consists of 4 parts:
		 * [0] Line number
		 * [1] Column number
		 * [2] Message type (error/warning)
		 * [3] Message text
		*/
		foreach($lines as $line) {
			$line = trim(substr($line, strpos($line, ':') + 1));
			if(is_numeric(substr($line, 0, 1))) {
				$parts = explode(':', $line);
				if(count($parts) === 4) array_push($errors, $parts);
			}
		}
		
		$feedback = array();
		$analytics = Analytics::instance();	

		// TODO omitting those errors whose templates aren't present			
		foreach($errors as $error) {
			$line = trim($error[0]);
			$position = trim($error[1]);
			$type = trim($error[2]);
			$message = trim($error[3]);
			
			if($type === 'note') $type = 'info';
			
			$analytic = $analytics->getErrorHash($message);
			$rows = Helper::cacheData("SELECT type,feedback FROM ext_feedback WHERE hash=?", array($analytic['hash']), 5);
			
			if(count($rows)) {
				
				$parseType = intval($rows[0]['type']);
				$feedbackMessage = $rows[0]['feedback'];
				
				if($parseType === 0) {
					foreach(array_keys($analytic['vars']) as $var) {
						$feedbackMessage = str_replace($var, $analytic['vars'][$var], $feedbackMessage);
					}
				} else {
					$v8 = new V8Js();
					$definitions = '';
					foreach(array_keys($analytic['vars']) as $var) {
						$definitions = $definitions . 'var ' . substr($var, 1) . '="' . $analytic['vars'][$var] . '";';
						$feedbackMessage = str_replace($var, substr($var, 1), $feedbackMessage);
					}
					$feedbackMessage = $definitions . $feedbackMessage;
					try {
						ob_start();
						$v8->executeString($feedbackMessage);
						$feedbackMessage = ob_get_clean();
					} catch(Exception $e) {
						ob_get_clean();
					}
				}
			} else 
				$feedbackMessage = $message;
			
			array_push($feedback, array(
				'type'=>$type,
				'line'=>$line,
				'position'=>$position,
				'feedback'=>$feedbackMessage
			));
		}
		
		return $feedback;
	}
	
	public function getExecutionFeedback($status_code, $executable, $testcase) {
			
		if(!in_array($status_code, array('RT', 'RF', 'TL', 'AT', 'ML'))) return null;
		
		$debugger = new Debugger($executable);
		
		if($status_code === 'RT') {
			// Runtime error occurred
			chdir(PATH_APPDATA);
			$debugger->debug_init();
			$test_file = generate_random_string() . '.test';
			file_put_contents($test_file, $testcase);
			$error = $debugger->catch_signal("r < " . $test_file);
			$debugger->debug_stop();
			unlink($test_file);
			chdir(BASE_DIR);
			return array('type'=>'RT', 'details'=>$error);
		} else if($status_code === 'RF') {
			// Restricted function call
		} else if($status_code === 'TL') {
			// Time limit exceeded
		} else if($status_code === 'AT') {
			// Abnormal termination occurred
		} else if($status_code === 'ML') {
			// Memory limit exceeded
		}
		
		return null;
	}
	
}
 
?>