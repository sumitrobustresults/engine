<?php

/**
 * Used to manage the system queue using MongoDB. Schema format:
 * 1. type
 * 2. reference
 * 3. retrieve_time
 * 4. in_process
 * 
 * @author Rajdeep Das <rajdeepd@iitk.ac.in>
 *
 */
class Queue {
	
	private $collectionName = "jqueue";
	private $client = null;
	private $db = null;
	
	public static function instance() {
		return new self();
	}
	
	public function __construct() {
		
		$host = $_SERVER['NOSQL_HOST'];
		$user = $_SERVER['NOSQL_USER'];
		$pass = $_SERVER['NOSQL_PASS'];
		
		$databaseName = $_SERVER['NOSQL_DB'];
		
		$connString = "mongodb://$user:$pass@$host:27017/$databaseName";
		
		$this->client = new MongoClient($connString);
		
		$this->db = $this->client->selectDB($databaseName);
	}
	
	public function getPending($jobType) {
		
		$col = $this->db->selectCollection($this->collectionName);
		
		return $col->count(array('type'=>$jobType));
	}
	
	public function retrieve($jobType) {
		
		$col = $this->db->selectCollection($this->collectionName);
		
		$job = $col->findAndModify(
			array('type'=>$jobType, 'in_process'=>0),
			array('$set'=>array('in_process'=>1, 'retrieve_time'=>new MongoDate())),
			null,
			null
		);
		
		return $job;
	}
	
	public function complete($jobType, $reference, $result) {
		
		$col = $this->db->selectCollection($this->collectionName);
		
		$result = $col->update(
			array('type'=>$jobType, 'reference'=>$reference),
			array('$set'=>array('in_progress'=>1, 'complete'=>1, 'complete_time'=>new MongoDate(), 'result'=>$result))
		);
		
		return $result;
	}
	
	public function getResult($jobType, $reference) {
		
		$col = $this->db->selectCollection($this->collectionName);
		
		$result = $col->findOne(
			array('type'=>$jobType, 'reference'=>$reference)
		);
		
		return $result;
	}
	
	public function add($jobType, $reference, $payload) {
		
		$col = $this->db->selectCollection($this->collectionName);
		
		$data = array(
			'type'=>$jobType,
			'reference'=>$reference,
			'complete'=>0,
			'in_process'=>0,
			'retrieve_time'=>null,
			'payload'=>$payload
		);
		
		$col->insert($data);
		
		return $data;
	}
	
	public function remove($jobType, $reference) {
		
		$col = $this->db->selectCollection($this->collectionName);
		
		$result = $col->remove(array(
			'type'=>$jobType, 
			'reference'=>$reference
		));
		
		return $result;
	}
	
}

?>