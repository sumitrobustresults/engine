<?php 
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Logger {
	
	private $enabled = false;
	
	public static function instance() {
		return new self();
	}
	
	public function __construct() {
		$enabled = intval(Registry::settings('TOOLS', 'logging'));
		if($enabled === 1)
			$this->enabled = true;
		else 
			$this->enabled = false;
	}
	
	/**
	 * Logs the results of compilation of a program.
	 *
	 * Tables used: compilation
	 */
	public function logCompilationResult($assignment_id, $code_id, $raw_output, $compiled) {
		
		if(!$this->enabled)
			return;
		
		$compilation = R::dispense('compilation');
	
		$compilation->assignment_id = $assignment_id;
		$compilation->code_id = $code_id;
		$compilation->raw_output = $raw_output;
		$compilation->compiled = $compiled;
		$compilation_id = R::store($compilation);
		$errors = array();
		$lines = array_filter(explode("\n", $raw_output));
		/**
		 * Each valid message consists of 4 parts:
		 * [0] Line number
		 * [1] Column number
		 * [2] Message type (error/warning)
		 * [3] Message text
		*/
		foreach($lines as $line) {
			$line = trim(substr($line, strpos($line, ':') + 1));
			if(is_numeric(substr($line, 0, 1))) {
				$parts = explode(':', $line);
				if(count($parts) === 4) array_push($errors, $parts);
			}
		}
		
		$data = array();
		
		foreach($errors as $error) {
			$line = trim($error[0]);
			$position = trim($error[1]);
			$type =     trim($error[2]);
			$message = trim($error[3]);
				
			array_push($data, sprintf("(%s,%s,%s,%s,'%s','%s')", 
				$compilation_id, $code_id, intval($line), intval($position), addslashes($message), $type));
		}
	
		$query = sprintf("INSERT INTO compilation_error (compilation_id,code_id,line,position,message,type)
			VALUES %s", implode(",", $data));
		if(count($data))
			R::exec($query);
		
		return $compilation_id;
	}
	
	/**
	 * Logs the results of execution of a program.
	 *
	 * Tables used: execution
	 */
	public function logExecutionResult($assignment_id, $result, $input, $output) {
		
		if(!$this->enabled)
			return;
		
		$rows = R::getAssocRow("SELECT id,code_id FROM compilation WHERE assignment_id=? ORDER BY compile_time DESC LIMIT 1", array($assignment_id));
	
		if(!count($rows)) return null;
	
		$compilation = $rows[0];
	
		$exec = R::dispense('execution');
	
		$exec->compilation_id = $compilation['id'];
		$exec->code_id = $compilation['code_id'];
		$exec->result = $result;
		$exec->input = $input;
		$exec->output = $output;
		return R::store($exec);
	}
	/**
	 * Logs the results of code evaluation on test cases.
	 * 
	 * @param int $assignment_id
	 * @param int $code_id
	 * @param array $results
	 * @return void
	 */
	public function logEvaluationResult($assignment_id, $code_id, $results) {
		
		if(!$this->enabled)
			return;
		
		if(Registry::lookupCustomConfig('type') === 'compiled') {
			$rows = R::getAssocRow("SELECT id,code_id FROM compilation WHERE assignment_id=? ORDER BY compile_time DESC LIMIT 1", array($assignment_id));
			if(!count($rows)) return null;
			$compilation_id = $rows[0]['id'];
			$code_id = $rows[0]['code_id'];
		} else {
			$rows = R::getAssocRow("SELECT id FROM code WHERE assignment_id=? ORDER BY save_time DESC LIMIT 1", array($assignment_id));
			if(!count($rows)) return null;
			$compilation_id = null;
			$code_id = $rows[0]['id'];
		}
	
		$accepted = true;
		
		$data = array();
	
		foreach($results as $result) {
			
			array_push($data, sprintf("(%s,%s,%s,%s,'%s','%s','%s')", 
				$assignment_id, $compilation_id, $code_id, $result['id'], $result['output'], $result['result'], $result['verdict']));
				
			if($result['verdict'] === 'WRONG_ANSWER')
				$accepted = false;
		}
		
		$query = sprintf("INSERT INTO evaluation (assignment_id,compilation_id,code_id,testcase_id,output,result,verdict) VALUES %s", 
			implode(",", $data));
		
		if(count($data))
			R::exec($query);
		R::exec("INSERT INTO attempt (code_id,assignment_id,success) VALUES (?,?,?)",
		array($code_id, $assignment_id, ($accepted) ? 1:0));
	}

}
?>