<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Job {
		
	public function doEventMemoization($event_id, $admin_id) {
		
		global $LOGGER;
		
		$event = R::load('event', $event_id);
		if(!$event->id) return false;
		$job = R::getAssocRow("SELECT * FROM job WHERE reference=?", array($event_id));
		// Only start a job
		if(count($job) > 0) return false;
		
		// Fetch the total number of assignments
		$query = "SELECT COUNT(*) AS num_assignments FROM assignment WHERE event_id=?";
		$rows = R::getAssocRow($query, array($event_id));
		$numAssignments = $rows[0]['num_assignments'];
		
		// Fetch the number of pending assignments
		$pending = array();
		$query = "SELECT assignment.id AS assignment_id
			FROM assignment 
			WHERE assignment.id NOT IN (SELECT assignment_id FROM memoized UNION SELECT reference AS assignment_id FROM defective WHERE type='MEMOIZATION_ASSIGNMENT') AND event_id=?";
		$rows = R::getAssocRow($query, array($event_id));
		foreach($rows as $row) array_push($pending, $row['assignment_id']);
		
		// Create job instance
		$job = R::dispense('job');
		$job->initiator = $admin_id;
		$job->reference = $event_id;
		$job->description = 'test-case memoization';
		$job->pieces = $numAssignments;
		$job->done = 0;
		$jid = R::store($job);
		
		// Get availability of workers
		$gears = array_filter(explode("\n", shell_exec("gearadmin --status")));
		foreach($gears as $gear) {
			$details = preg_split("/\\s+/", $gear);
			if(!count($details) || $details[0] !== 'judge') continue;
			$total = intval($details[3]);
			$queue = intval($details[1]);
			$working = intval($details[2]);
			// Assign based on all available workers
			$available = $total; 
		}
		
		if($available == 0) return false;
		
		// Chunk the pieces
		$bucketSize = floor($numAssignments / $available);
		$chunks = array_chunk($pending, $bucketSize);
		
		$client = new GearmanClient();
		$client->addServer();
		
		$handles = array();
		
		// Start tasks
		foreach($chunks as $chunk) {
			
			$uid = uniqid('', true);
			$uid = str_replace('.', '_', $uid);
			
			$task = R::dispense('chunk');
			$task->job_id = $jid;
			$task->uid = $uid;
			$task->pieces = count($chunk);
			$task->processed = 0;
			$tid = R::store($task);
			
			$wl = array(
				'jid'=>$jid,
				'tid'=>$tid,
				'load'=>$chunk
			);
			
			$handle = $client->doBackground('judge', json_encode($wl), $uid);
			
			array_push($handles, $handle);
			
			$task->handle = $handle;
			$tid = R::store($task);
			
			$client->doBackground('monitor', json_encode(array('task_id'=>$tid)));
		}
		
		return $handles;
	}
	
	public function redoEventMemoization($task_id) {
		
		$task = R::load('chunk', $task_id);
		
		$job_file = sprintf('%sworkers/handles/task_%s.job', BASE_DIR, $task->uid);
		$job_details = json_decode(file_get_contents($job_file), true);
		
		$assignments_list = $job_details['pieces'];
		$job_id = $job_details['job_id'];
		
		$wl = array(
			'jid'=>$job_id,
			'tid'=>$task_id,
			'load'=>$assignments_list
		);
		
		$client = new GearmanClient();
		$client->addServer();
		
		$status = $client->jobStatus($task->handle);
		if($status[0] && $status[1]) return null;
		
		$handle = $client->doBackground('judge', json_encode($wl), $task->uid);
		
		$task->handle = $handle;
		R::store($task);
		
		return $handle;
	}
	
	public function resumeEventMemoization($event_id) {
		
		$query = "SELECT id FROM chunk WHERE job_id=(SELECT id FROM job WHERE reference=?) AND pieces<>processed";
		
		$tasks = R::getAssocRow($query, array($event_id));
		
		$success = true;
		
		foreach($tasks as $task) {
			$handle = $this->redoEventMemoization($task['id']);
			if(!$handle) $success = false;
		}
		
		return $success;
	}
	
	public function getEventMemoizationStatus($event_id) {
		
		global $LOGGER;
		
		$job = R::getAssocRow("SELECT * FROM job WHERE reference=?", array($event_id));
		
		if(count($job) > 0) {
			$job = $job[0];
			$complete = intval($job['done']);
			$total = intval($job['pieces']);
			
			$rows = R::getAssocRow("SELECT id FROM defective WHERE reference IN (SELECT id FROM assignment WHERE event_id=?)", array($job['reference']));
			$numDefectives = count($rows);
			
			$pending = $total - $complete - $numDefectives;
			
			$client = new GearmanClient();
			$client->addServer();
			
			$running = false;
			$processed = 0;
			$gears = array();
			$start = strtotime($job['start']);
			$now = time();
			$tasks = R::getAssocRow("SELECT * FROM chunk WHERE job_id=?", array($job['id']));
			foreach($tasks as $task) {
				$processed += $task['processed'];
				$handle = $task['handle'];
				$status = $client->jobStatus($handle);
				if($status[0] && $status[1]) $running = true;
				array_push($gears, array(
					'running'=>($status[0] && $status[1]), 
					'processed'=>$task['processed']
				));
			}
			if($now == $start) $now = $start + 1000;
			$rate = $processed / ($now - $start);
			
			$initiated = true;
		} else {
			$gears = null;
			$job = null;
			$query = "SELECT id FROM assignment WHERE event_id=?";
			$qkey = 'Q' . base64_encode(str_replace('?', $event_id, $query));
			$memcached = new Memcached();
			$memcached->addServer('127.0.0.1', 11211);
			if(($value = $memcached->get($qkey)) !== false) {
				$rows = json_decode($value, true);
			} else {
				$rows = R::getAssocRow($query, array($event_id));
				$memcached->set($qkey, json_encode($rows), time() + 1800);
			}
			$pending = count($rows);
			$total = $pending;
			$complete = 0;
			$rate = 0;
			$initiated = false;
			$running = false;
		}
		
		return array(
			'event_id'=>$event_id,
			'total'=>$total,
			'pending'=>$pending,
			'complete'=>$complete,
			'initiated'=>$initiated,
			'running'=>$running,
			'job'=>$job,
			'rate'=>$rate,
			'gears'=>$gears
		);
	}
	
	public function checkConsistencyInMemoization($event_id) {
		
		$query = "SELECT COUNT(*) AS num_assignments FROM assignment WHERE event_id=?";
		$rows = R::getAssocRow($query, array($event_id));
		$numAssignments = $rows[0]['num_assignments'];
		
		$query = "SELECT DISTINCT assignment_id FROM evaluation WHERE result='CE' AND assignment_id IN (SELECT id FROM assignment WHERE event_id=?)";
		$notcompiled = R::getAssocRow($query, array($event_id));
		
		$query = "SELECT assignment.id AS assignment_id
			FROM assignment
			WHERE assignment.id NOT IN (SELECT assignment_id FROM memoized UNION SELECT reference AS assignment_id FROM defective WHERE type='MEMOIZATION_ASSIGNMENT') AND event_id=?";
		$untouched = R::getAssocRow($query, array($event_id));
		
		// Considers non-submitted assignments as well.
		$query = "SELECT assignment.id AS assignment_id,testcase.id AS test_id,assignment.problem_id AS problem_id
			FROM assignment INNER JOIN testcase
			ON assignment.problem_id=testcase.problem_id
			WHERE (assignment.id,testcase.id) NOT IN (SELECT assignment_id,test_id FROM memoized) AND event_id=?";
		
		$pending = R::getAssocRow($query, array($event_id));
		
		$query = "SELECT reference FROM defective WHERE type='MEMOIZATION_ASSIGNMENT' AND reference IN (SELECT id FROM assignment WHERE event_id=?)";
		
		$defective = R::getAssocRow($query, array($event_id));
		
		$query = "SELECT assignment.id AS assignment_id,testcase.id AS test_id,assignment.problem_id AS problem_id
			FROM assignment INNER JOIN testcase
			ON assignment.problem_id=testcase.problem_id
			WHERE (assignment.id,testcase.id) IN (SELECT assignment_id,test_id FROM memoized) AND event_id=?";
		
		$complete = R::getAssocRow($query, array($event_id));
		
		$classified = array();
		$exclusions = array('EX'=>array(), 'ER'=>array(), 'CE'=>array());
		
		foreach($pending as $item) {
			if(!array_key_exists($item['assignment_id'], $classified)) $classified[$item['assignment_id']] = array('fault'=>'U', 'tests'=>array());
			array_push($classified[$item['assignment_id']]['tests'], $item['test_id']);
		}
		
		$numPending = count(array_keys($classified));
		$numNotCompiled = count($notcompiled);
		$numUntouched = count($untouched);
		
		foreach(array_keys($classified) as $aid) {
			$result = R::getAssocRow("SELECT result FROM evaluation WHERE assignment_id=? ORDER BY id DESC LIMIT 1", array($aid));
			if(!count($result)) continue;
			$result = $result[0]['result'];
			$classified[$aid]['fault'] = $result;
			if($result === 'EX') {
				foreach($classified[$aid]['tests'] as $tid) array_push($exclusions['EX'], array($aid, $tid));
			} else if($result === 'ER') {
				foreach($classified[$aid]['tests'] as $tid) array_push($exclusions['ER'], array($aid, $tid));
			} else if($result === 'CE') {
				foreach($classified[$aid]['tests'] as $tid) array_push($exclusions['CE'], array($aid, $tid));
			}
		}
		
		$fail_db = count($exclusions['EX']);
		$fail_sys = count($exclusions['ER']);
		$fail_cmp = count($exclusions['CE']);
		
		$unaccounted = count($pending) - ($fail_cmp + $fail_db + $fail_sys);
		
		return array(
			'event_id'=>$event_id,
			'assignments'=>array(
				'complete'=>($numAssignments - $numPending),
				'partial'=>($numPending - $numUntouched),
				'defective'=>count($defective),
				'notcompiled'=>$numNotCompiled,
				'pending'=>($numUntouched - $numNotCompiled),
				'total'=>$numAssignments
			),
			'units'=>array(
				'complete'=>count($complete),
				'database_fail'=>$fail_db,
				'system_fail'=>$fail_sys,
				'compilation_fail'=>$fail_cmp,
				'unaccounted'=>$unaccounted,
				'total'=>(count($pending) + count($complete))
			)
		);
	}
	
	function cleanMemoization($event_id) {
		
		$query = "DELETE FROM memoized WHERE assignment_id IN (SELECT id FROM assignment WHERE event_id=?)";
		R::exec($query, array($event_id));
		
		$query = "DELETE FROM evaluation WHERE assignment_id IN (SELECT id FROM assignment WHERE event_id=?)";
		R::exec($query, array($event_id));
		
		$query = "DELETE FROM job WHERE reference=?";
		R::exec($query, array($event_id));
		
		return true;
	}
	
}

?>