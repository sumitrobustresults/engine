<?php
/**
 * Copyright (c) 2014 Rajdeep Das.
 * All rights reserved.
 *
 * Usage of this program and the accompanying materials in any form
 * without prior permission from the owner is strictly prohibited.
 *
 * Author(s): Rajdeep Das <rajdeepd@iitk.ac.in>
 */

class Tests {
	
	public static function instance() {
		return new self();
	}
	
	public function testCompile() {
		
		$build_name = md5(rand() . "" . time());
		$source_file = PATH_APPDATA . $build_name . ".c";
		$executable = PATH_APPDATA . $build_name . ".out";
		
		$code = '#include <stdio.h> 
			int main() {
				printf("Test Output\n"); 
				return 0;
			}';
		
		$output = Engine::instance()->compile($build_name, $code);
		
		if(file_exists($source_file))
			unlink($source_file);
		if(file_exists($executable))
			unlink($executable);
		
		return $output;
	}
	
	public function testExecute() {
		
		$build_name = md5(rand() . "" . time());
		$source_file = PATH_APPDATA . $build_name . ".c";
		$executable = PATH_APPDATA . $build_name . ".out";
		
		$code = '#include <stdio.h>
			int main() {
				int LIMIT = 15000;
				int i,j,isPrime,primes;
				primes = 0;
				for(i = 2; i < LIMIT; i++) {
					isPrime = 1;
					for(j = 2; j < i; j++) {
						if(i % j == 0)
							isPrime = 0;
					}
					if(isPrime)
						primes++;
				}
				printf("Primes: %d\n", primes);
				return 0;
			}';
		
		$compilation = Engine::instance()->compile($build_name, $code);
		
		if($compilation['result'] === 'success')
			$execution = Engine::instance()->execute($build_name . ".out", "");
		else
			return $compilation;
		
		if(file_exists($source_file))
			unlink($source_file);
		if(file_exists($executable))
			unlink($executable);
		
		return $execution;
	}
	
	public function testEvaluate() {
		
		$NUM_TESTS = 10;
		$results = array();
		
		for($i = 0; $i < $NUM_TESTS; $i++) {
			$result = $this->testExecute();
			array_push($results, $result);
		}
		
		return $results;
	}
	
	public function testDatabaseWrite() {
		
		$query = "INSERT INTO _test (msg) VALUES (?)";
		R::exec($query, array(md5(rand())));
		
		return true;
	}
	
	public function testDatabaseRead() {
		
		$query = "SELECT msg FROM _test WHERE msg REGEXP '^[0-9].*'";
		$rows = R::getAssocRow($query);
		
		return $rows;
	}
	
}
?>